// soal 1
// "saya senang belajar JAVASCRIPT"
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

console.log(pertama.substr(0,5).concat(pertama.substr(12,7)).concat(kedua.substr(0,8)).concat(kedua.substr(8,10).toUpperCase()))


// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

// ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
// *catatan :
// 1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
// 2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)
console.log(parseInt(kataPertama)+(parseInt(kataKedua) * parseInt(kataKetiga)) + parseInt(kataKeempat))


// soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(kataPertama.length+1, 10)
var kataKetiga = kalimat.substr(kataPertama.length + kataKedua.length +2, 3)
var kataKeempat = kalimat.substr(kalimat.indexOf(kataKetiga)+kataKetiga.length+1, 6) // do your own! 
var kataKelima = kalimat.substring(kalimat.indexOf(kataKeempat) + kataKeempat.length)

//selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:
//Kata Pertama: wah
//Kata Kedua: javascript
//Kata Ketiga: itu
//Kata Keempat: keren
//Kata Kelima: sekali

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

