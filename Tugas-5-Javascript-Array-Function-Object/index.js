// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
console.log(daftarHewan)
for (var i in daftarHewan) {
    console.log(daftarHewan[i])
}

// soal 2
function introduce(data) {
    return "Nama saya " + data.name + ", Umur saya " + data.age + " tahun, alamat saya di " + data.address +
    ", dan saya punya hobi yaitu " + data.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan)
// Menampilkan "Nama saa John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


// soal 3
function hitung_huruf_vokal(str) {
    str = str.toLowerCase().split('')
    a = str.filter(function(item){
        return (item == "a" | item == "e" | item == "i" | item == "o" | item == "u");
    })
    return a.length
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2


// soal 4
function hitung(int) {
    return 2*int - 2    
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8