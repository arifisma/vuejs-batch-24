// soal 1
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"

function jumlah_kata(kalimat) {
    console.log(kalimat.trim().split(" ").length)
}

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2


// soal 2
function next_date(tgl, bln, thn) {
    //mengembalikan nilai tanggal hari esok dalam bentuk string
    if (tgl < 1 | tgl > 31) {
        return console.log("tgl tidak valid")
    } else if (bln < 1 | bln > 12) {
        return console.log("bln tidak valid")
    }

    bln_31 = [1,3,5,7,8,10,12]
    bln_30 = [4,6,9,11]
    if (bln_31.includes(bln)) { // untuk tgl maks 31
        if (tgl == 31){
            tgl = 1
            bln += 1
        } else {
            tgl +=1
        }
    } else if (bln_30.includes(bln)) {
        if (tgl > 30) { return console.log("tgl tidak valid");}
        if (tgl >= 30){
            tgl = 1
            bln += 1
        } else {
            tgl +=1
        }
    } else { //bulan februari
        if (thn % 4 == 0) {
            if (tgl > 29) { return console.log("tgl tidak valid");}
            else if (tgl == 29) {
                tgl = 1;
                bln +=1
            } else {
                tgl +=1
            }
        } else {
            // bukan kabisat
            if (tgl > 28) { return console.log("tgl tidak valid");}
            if (tgl == 28) {
                tgl =1
                bln+=1    
            } else {
                tgl+=1
            }
            
        }
    }

    if (bln==13) {
        bln=1;
        thn+=1
    }
    switch(bln) { //merubah int bulan menjadi str
        case 1:   {bln = "Januari"; break; }
        case 2:   {bln= "Februari"; break; }
        case 3:   { bln = "Maret"; break; }
        case 4:   { bln = "April"; break; }
        case 5:   { bln = "Mei"; break; }
        case 6:   { bln = "Juni"; break; }
        case 7:   { bln = "Juli"; break; }
        case 8:   { bln = "Agustus"; break; }
        case 9:   { bln = "September"; break; }
        case 10:   { bln = "Oktober"; break; }
        case 11:   { bln = "November"; break; }
        case 12:   { bln = "Desember"; break; }
        default:  { console.log('Bulan tidak valid'); }}
        
    console.log(tgl + " " + bln + " " + thn)
}


var tanggal = 25
var bulan = 2
var tahun = 2021
console.log(next_date(tanggal, bulan, tahun))