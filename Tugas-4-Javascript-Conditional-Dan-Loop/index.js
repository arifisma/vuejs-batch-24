// soal 1
var nilai = 76;
if (nilai >= 85) {
    console.log("indeksnya A")
} else if ( nilai >= 75 && nilai < 85) {
    console.log("indeksnya B")
} else if ( nilai >= 65 && nilai < 75) {
    console.log("indeksnya C")
} else if ( nilai >= 55 && nilai < 65) {
    console.log("indeksnya D")
} else {
    console.log("indeksnya E")
}


// soal 2
var tanggal = 1;
var bulan = 12;
var tahun = 2020;

switch(bulan) {
  case 1:   { console.log(tanggal + " Januari " + tahun); break; }
  case 2:   { console.log(tanggal + " Februari " + tahun); break; }
  case 3:   { console.log(tanggal + " Maret " + tahun); break; }
  case 4:   { console.log(tanggal + " April " + tahun); break; }
  case 5:   { console.log(tanggal + " Mei " + tahun); break; }
  case 6:   { console.log(tanggal + " Juni " + tahun); break; }
  case 7:   { console.log(tanggal + " Juli " + tahun); break; }
  case 8:   { console.log(tanggal + " Agustus " + tahun); break; }
  case 9:   { console.log(tanggal + " September " + tahun); break; }
  case 10:   { console.log(tanggal + " Oktober " + tahun); break; }
  case 11:   { console.log(tanggal + " November " + tahun); break; }
  case 12:   { console.log(tanggal + " Desember " + tahun); break; }
  default:  { console.log('Bulan tidak valid'); }}

// soal 3
var n = 10;
var i = 1;
while(i <= n) {
  console.log("#".repeat(i));
  i++;
}

// soal 4
var m = 17;
for(var n = 1; n <= m; n++) {
    if (n < 4) {
        switch(n) {
            case 1:   { console.log(n + " - I love programming"); break; }
            case 2:   { console.log(n + " - I love Javascript"); break; }
            case 3:   { console.log(n + " - I love VueJS");
                        console.log("======");
                        break; }
            //default:  { console.log('???'); }
        }
    } else {
        a = n % 4
        switch(a) {
            case 0:   { console.log(n + " - I love programming"); break; }
            case 1:   { console.log(n + " - I love Javascript"); break; }
            case 2:   { console.log(n + " - I love VueJS");
                        console.log("======");
                        break; }
            //default:  { console.log('???'); }
        }
     
    }
}