// soal 1
let panjang = 20
let lebar = 10

const luasPersegiPanjang = (panjang=10, lebar=5) => {
    return panjang * lebar
}

a = luasPersegiPanjang(panjang, lebar)
console.log(a)


// soal 2: object literal
const literal = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
      }
    }
  }
   
literal("William", "Imoh").fullName()
console.log(literal("William", "Imoh").firstName)


// soal 3: destructuring
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football"
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)


// soal 4: array spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// ES 5 way
//const combined = west.concat(east)
// ES 6 way: array spreading
const combined = [...west, ...east]
console.log(combined)


// soal 5: template literal
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`
console.log(before)