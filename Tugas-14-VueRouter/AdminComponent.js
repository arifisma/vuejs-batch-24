export const AdminComponent = {
    data () {
        return {
            admins: [
                {
                    id: 1,
                    name: 'Admin1'
                },
                {
                    id: 2,
                    name: 'Admin2'
                },
                {
                    id: 3,
                    name: 'Admin3'
                },
                
            ]
        }
    },
    template: `
        <div>
            <h1>Daftar Admin</h1>
            <ul>
                <li v-for="admin of admins">
                    <router-link :to="'/admin/'+admin.id"> 
                        {{ admin.name }} 
                    </router-link>
                </li>
            </ul>
        </div>
    ` 
}